package com.example.evzparadise.hackernews;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ProgressBar;

import com.oc.hnapp.android.HNArticle;
import com.oc.hnapp.android.HNQueryTask;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private List<HNArticle> articles = new ArrayList<>();
    private RecyclerView recyclerView;
    private HNArticlesAdapter aAdapter;
    private HNQueryTask task = null;
    private ProgressBar progressBar = null;
    private ProgressBar progress = null;
    private int page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EventBus.getDefault().register(this);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        aAdapter = new HNArticlesAdapter(this, this);
        RecyclerView.LayoutManager aLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(aLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(aAdapter);

        loadNext();

        progress = (ProgressBar) findViewById(R.id.progress);
        aAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                if(progress != null) {
                    progress.setVisibility(View.GONE);
                }
            }
        });


    }

    public void loadNext() {
        if (task != null && task.getStatus() != AsyncTask.Status.FINISHED)
            return ;

        if(page == 0 ) {
            progressBar.setVisibility(View.VISIBLE);
        }
        task = new HNQueryTask(aAdapter, 80, ++page);
        task.execute();

    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
        task.cancel(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoadingMessageComplet(LoadingMessageComplet event) {
        progressBar.setVisibility(View.GONE);
    };

}
